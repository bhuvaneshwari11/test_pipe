FROM alpine:3.9

RUN apk add --update --no-cache bash

RUN mkdir -p /src

COPY pipe.sh /src

WORKDIR /src
 
RUN chmod a+x pipe.sh

ENTRYPOINT ["sh","/src/pipe.sh"]
